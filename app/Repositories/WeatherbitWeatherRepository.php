<?php

namespace App\Repositories;

use App\Contracts\WeatherRepository;
use App\Exceptions\WeatherException;
use App\Http\Resources\ForecastCollection;
use App\Http\Resources\WeatherCollection;
use GuzzleHttp\Client;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Collection;

class WeatherbitWeatherRepository implements WeatherRepository
{
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function showForecast(array $cities, int $days): Collection
    {
        return collect(Pool::batch($this->client,
            array_map(function ($city) use ($days) {
                return function () use ($city, $days) {
                    return $this->client->getAsync('forecast/daily', [
                        'query' => [
                            'days' => $days,
                            'city' => $city,
                            'country' => 'AU',
                            'key' => env('WEATHERBIT_API_KEY'),
                        ]
                    ]);
                };
            }, $cities), ['concurrency' => 3]))->map(function ($response) {
            if (!($response instanceof Response) || $response->getStatusCode() !== 200) {
                return new WeatherException();
            }

            return new WeatherCollection(json_decode($response->getBody())->data);
        });
    }
}
