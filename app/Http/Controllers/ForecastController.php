<?php

namespace App\Http\Controllers;

use App\Contracts\WeatherRepository;

class ForecastController extends Controller
{
    protected $weatherRepository;

    public function __construct(WeatherRepository $weatherRepository)
    {
        $this->weatherRepository = $weatherRepository;
    }

    public function showForm()
    {
        return view('forecastForm');
    }

    public function getForecast(string $city)
    {
        $forecast = $this->weatherRepository->showForecast((array)$city, 5)->first();

        if ($forecast instanceof \Exception) {
            return response()
                ->json(['success' => false, 'message' => $forecast->getMessage()]);
        }

        return response()
            ->json(['success' => true, 'data' => $forecast]);
    }
}
