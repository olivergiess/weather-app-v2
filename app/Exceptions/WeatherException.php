<?php

namespace App\Exceptions;

use Exception;

class WeatherException extends Exception
{
    public function __construct()
    {
        parent::__construct('Unable to retrieve forecast for city.');
    }
}
