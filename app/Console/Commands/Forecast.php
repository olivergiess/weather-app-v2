<?php

namespace App\Console\Commands;

use App\Contracts\WeatherRepository;
use Carbon\CarbonPeriod;
use Illuminate\Console\Command;

class Forecast extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'forecast
                            {cities : Comma delimited string of Cities. }
                            {days=5 : Number of days to retrieve (maximum 16). }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Output tabulated weather forecast for a list of cities over N days.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(WeatherRepository $repository)
    {
        $this->repository = $repository;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $days = (int)$this->argument('days');

        if($days > 16) {
            $this->error('Maximum number of days exceeded.');
            $this->info('Days set to 16.');

            $days = 16;
        }

        $cities = explode(',', $this->argument('cities'));

        $this->table($this->getHeaders($days), $this->getRows($cities, $days));
    }

    private function getHeaders(string $days): array
    {
        return array_merge(['City'], array_map(function ($day) {
            return $day->format('l');
        }, (new CarbonPeriod('now', "today +$days days"))->toArray()));
    }

    private function getRows(array $cities, string $days): array
    {
        return $this->repository->showForecast($cities, $days)->map(function ($forecast) use ($cities) {
            $city = trim(array_shift($cities));

            if ($forecast instanceof \Exception) {
                return [$city, $forecast->getMessage()];
            }

            return array_merge([$city], array_column($forecast->resolve(), 'status'));
        })->toArray();
    }
}
