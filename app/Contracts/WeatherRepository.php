<?php

namespace App\Contracts;

use Illuminate\Support\Collection;

interface WeatherRepository
{
    public function showForecast(array $cities, int $days): Collection;
}
