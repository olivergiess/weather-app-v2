<?php

namespace App\Providers;

use App\Http\Resources\WeatherCollection;
use GuzzleHttp\Client;
use GuzzleHttp\Pool;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Contracts\WeatherRepository', function ($app) {
            return new \App\Repositories\WeatherBitWeatherRepository(new Client([
                'base_uri' => 'https://api.weatherbit.io/v2.0/'
            ]));
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        new Client([
            'base_uri' => 'https://api.weatherbit.io/v2.0/'
        ]);
    }
}
