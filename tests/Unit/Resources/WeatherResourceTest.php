<?php

namespace Tests\Unit;

use App\Http\Resources\WeatherResource;
use Tests\TestCase;

class WeatherResourceTest extends TestCase
{
    public function testReturnsCollection() {

        $this->assertJson((new WeatherResource((object)[
            'valid_date' => '1970-01-01',
            'weather' => (object)['description' => 'Test']
        ]))->toJson(), json_encode([
            [
                'date' => '1970-01-01',
                'status' => 'Test'
            ]
        ]));
    }
}
