<?php

namespace Tests\Unit;

use App\Http\Resources\WeatherCollection;
use App\Http\Resources\WeatherResource;
use Tests\TestCase;

class WeatherCollectionTest extends TestCase
{
    public function testCollectsWeatherResources()
    {
        $this->assertInstanceOf(WeatherResource::class,
            (new WeatherCollection([
                (object)[
                    'valid_date' => '1970-01-01',
                    'weather' => (object)['description' => 'Test']
                ]
            ]))->first());
    }
}
