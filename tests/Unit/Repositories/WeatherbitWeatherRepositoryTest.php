<?php

namespace Tests\Unit;

use App\Contracts\WeatherRepository;
use Illuminate\Support\Collection;
use Tests\TestCase;

class WeatherbitWeatherRepositoryTest extends TestCase
{
    public function testReturnsCollection() {
        $this->assertInstanceOf(Collection::class,
            $this->app->make(WeatherRepository::class)->showForecast(['Brisbane'], 1)
        );
    }
}
