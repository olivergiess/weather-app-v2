<?php

namespace Tests\Feature\Console;

use Tests\TestCase;

class ForecastTest extends TestCase
{
    public function testSuccessful()
    {
        $this->artisan("forecast 'Test' 5")->assertExitCode(0);
    }
}
