<?php

namespace Tests\Feature\Web;

use Tests\TestCase;

class ForecastTest extends TestCase
{
    public function testHasCorrectTitle()
    {
        $this->get('/')->assertSeeText('Weather App V2');
    }
}
