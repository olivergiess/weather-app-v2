<?php

namespace Tests\Feature\API;

use Tests\TestCase;

class ForecastTest extends TestCase
{
    public function testExpectedJsonResponse()
    {
        $this->get('/api/forecast/test')->assertJson([
            'success' => true,
            'data' => [
                [
                    'date' => '1970-01-01',
                    'status' => 'Test'
                ]
            ]
        ]);
    }
}
