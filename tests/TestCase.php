<?php

namespace Tests;

use App\Contracts\WeatherRepository;
use App\Http\Resources\WeatherCollection;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function setUp(): void
    {
        parent::setUp();

        $this->instance(WeatherRepository::class, \Mockery::mock(WeatherRepository::class, function ($mock) {
            $mock->allows([
                'showForecast' => collect([
                    [
                        (object)[
                            'valid_date' => '1970-01-01',
                            'weather' => (object)['description' => 'Test']
                        ]
                    ]
                ])->map(function ($temp) {
                    return new WeatherCollection($temp);
                })
            ]);
        }));
    }
}
