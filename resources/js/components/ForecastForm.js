import axios from 'axios';
import React, {useState} from 'react';
import ReactDOM from 'react-dom';

import AppCard from './ui/AppCard';
import AppSelect from './ui/inputs/AppSelect';
import AppSpinner from './ui/AppSpinner';

const ForecastForm = () => {
    const [city, setCity] = useState('');
    const [error, setError] = useState('');
    const [forecast, setForecast] = useState([]);
    const [loading, setLoading] = useState(false);

    const getWeatherByCity = city => {
        setCity(city);
        setError('');
        setForecast([]);

        if(!city) {
            return;
        }

        setLoading(true);

        axios.get(`/api/forecast/${city}`)
            .then(res => {
                const data = res.data

                if(data.success) {
                    setForecast(data.data)
                } else {
                    setError(data.message)
                }
            })
            .catch(() => setError('There was an error retrieving the forecast.'))
            .finally(() => setLoading(false));
    };

    const getDayName = date => new Date(date).toLocaleString('default', {weekday: 'long'});

    return (
        <AppCard>
            <div className="text-lg mb-2">
                Weather App V2
            </div>

            <div className="mb-2">
                {loading
                    ? <AppSpinner/>
                    : <AppSelect
                        items={['Failure', 'Brisbane', 'Gold Coast', 'Sunshine Coast']}
                        input={e => getWeatherByCity(e.target.value)}
                        value={city}
                    />
                }
            </div>

            { !!error && <div className="text-red-600">{error}</div>}

            {forecast.map(day =>
                <div className="inline-block px-2" key={day.date}>
                    <div className="font-semibold mb-2">{getDayName(day.date)}</div>
                    <div className="">{day.status}</div>
                </div>
            )}
        </AppCard>
    );
}

export default ForecastForm;

if (document.getElementById('forecast-form')) {
    ReactDOM.render(<ForecastForm/>, document.getElementById('forecast-form'));
}
