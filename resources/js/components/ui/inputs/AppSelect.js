import React from 'react';
import PropTypes from 'prop-types';

const AppSelect = ({ items, input, name, value }) => {
    return (
        <select
            className="outline-none w-full px-2 py-1 border"
            name={ name }
            onChange={ e => input(e) }
            value={ value }
        >
            <option value="">Select</option>
            { items.map(item => <option key={ item } value={ item }>{ item }</option>) }
        </select>
    );
};

AppSelect.propTypes = {
    items: PropTypes.array.isRequired,
    input: PropTypes.func,
    name: PropTypes.string,
    value: PropTypes.string
};

AppSelect.defaultProps = {
    input: e => {},
    name: '',
    value: ''
};

export default AppSelect;
