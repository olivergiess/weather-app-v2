import React from 'react';

const AppSpinner = () => {
    return (
        <div className="flex justify-around">
            <div className="app-loader h-6 w-6 rounded-full border-t border-teal-300">
            </div>
        </div>
    );
};

export default AppSpinner;
