import React from 'react';

const AppCard = ({ children }) => {
    return (
        <div className="px-6 py-4 bg-white rounded shadow-lg">
            { children }
        </div>
    );
};

export default AppCard;
