@extends('base')

@section('body')
    <div class="h-screen overflow-x-auto flex items-center justify-around">
        <div id="forecast-form"></div>
    </div>
@endsection
